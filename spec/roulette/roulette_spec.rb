# frozen_string_literal: true

require 'spec_helper'
require 'byebug'
require './app/roulette/roulette'

RSpec.describe RouletteGame, type: :model do
  context 'spin and result in a number between 1 and 36 or 0 or 00' do
    let(:params) { { players_in_game: 1 } }
    let(:roulette_game) { RouletteGame.new(params) }
    let(:possible_numbers) { (0..37).to_a }

    it { expect(possible_numbers.include?(roulette_game.spin)).to eq(true) }
  end
end
