# frozen_string_literal: true

require 'spec_helper'
# require 'require_all'
require 'byebug'
require './app/statistics'
require './app/roulette/roulette'
require './app/house'
require './app/bet/bet'
require './app/strategy/strategy'
require './app/player'
require './app/user_interface'
# require_all './app'

RSpec.shared_examples 'and a specific kind of bet(strategy)' do
  let(:override_player_atts) { { bet_number: kind_of_bet } }
  let(:pay_rate) { bet.pay_rate(kind_of_bet) }
  let(:proc_to_bet) { bet.get_block(kind_of_bet) }
  let(:bet_numbers) { bet.numbers_to_bet(player_bet_options, proc_to_bet) }
  let(:player_atts) do
    {
      rounds: 30,
      from_main: false,
      name: 'Danny McAskill',
      budget: 1_000,
      initial_bet: 1,
      strategy_number: player_strategy_number,
      strategy_options: player_strategy_options,
      bet_options: player_bet_options,
      bet_numbers: bet_numbers
    }
  end
  let(:player) do
    Player.new(player_atts)
  end
  let(:players) do
    {
      1.to_s.to_sym => player,
      2.to_s.to_sym => player2
    }
  end

  before { player.play(interface, house, roulette_game) }

  it do
    expect(statistics.print_statistics(roulette_game, last_round, house, bet, players)).to eq('Hola')
  end
end

RSpec.shared_examples 'with all kinds of bet (from strategy)' do
  context 'and Straight Bet' do
    let(:kind_of_bet) { 1 }
    let(:player_bet_options) do
      {
        number: 36,
        pay_rate: pay_rate
      }
    end

    include_examples 'and a specific kind of bet(strategy)'
  end

  context 'and Split Bet' do
    let(:kind_of_bet) { 2 }
    let(:player_bet_options) do
      {
        number1: 2,
        number2: 5,
        pay_rate: pay_rate
      }
    end

    include_examples 'and a specific kind of bet(strategy)'
  end

  context 'and Street Bet' do
    let(:kind_of_bet) { 3 }
    let(:player_bet_options) do
      {
        row: 12,
        pay_rate: pay_rate
      }
    end

    include_examples 'and a specific kind of bet(strategy)'
  end

  context 'and Corner Bet' do
    let(:kind_of_bet) { 4 }
    let(:player_bet_options) do
      {
        number1: 20,
        number2: 21,
        number3: 23,
        number4: 24,
        pay_rate: pay_rate
      }
    end

    include_examples 'and a specific kind of bet(strategy)'
  end

  context 'and Five Numbers Bet' do
    let(:kind_of_bet) { 5 }
    let(:player_bet_options) do
      {
        pay_rate: pay_rate
      }
    end

    include_examples 'and a specific kind of bet(strategy)'
  end

  context 'and Line Bet' do
    let(:kind_of_bet) { 6 }
    let(:player_bet_options) do
      {
        line: 1,
        pay_rate: pay_rate
      }
    end

    include_examples 'and a specific kind of bet(strategy)'
  end

  context 'and Range Bet' do
    let(:kind_of_bet) { 7 }
    let(:player_bet_options) do
      {
        which_twelve: 3,
        pay_rate: pay_rate
      }
    end

    include_examples 'and a specific kind of bet(strategy)'
  end

  context 'and Column Bet' do
    let(:kind_of_bet) { 8 }
    let(:player_bet_options) do
      {
        column: 2,
        pay_rate: pay_rate
      }
    end

    include_examples 'and a specific kind of bet(strategy)'
  end

  context 'and Low or High Bet' do
    let(:kind_of_bet) { 9 }
    let(:player_bet_options) do
      {
        low_or_high: 1,
        pay_rate: pay_rate
      }
    end

    include_examples 'and a specific kind of bet(strategy)'
  end

  context 'and Red or Black Bet' do
    let(:kind_of_bet) { 10 }
    let(:player_bet_options) do
      {
        red_or_black: 2,
        pay_rate: pay_rate
      }
    end

    include_examples 'and a specific kind of bet(strategy)'
  end

  context 'and Even or Odd Bet' do
    let(:kind_of_bet) { 11 }
    let(:player_bet_options) do
      {
        even_or_odd: 2,
        pay_rate: pay_rate
      }
    end

    include_examples 'and a specific kind of bet(strategy)'
  end
end

RSpec.shared_examples 'with a specific strategy' do
  let(:override_roulette_atts) { { players_in_game: number_of_players } }

  include_examples 'with all kinds of bet (from strategy)'
end

RSpec.describe Statistics, type: :model do
  let(:house) { House.new }
  let(:strategy) { Strategy.new(0) }
  let(:bet) { Bet.new }
  let(:max_rounds) { 200 }
  let(:interface) { UserInterface.new(bet, strategy) }
  let(:override_roueltte_atts) { {} }
  let(:roulette_atts) do
      override_roulette_atts
  end
  let(:override_player_atts) { {} }
  let(:player_atts) { {} }
  let(:roulette_game) { RouletteGame.new(roulette_atts) }
  let(:statistics) { Statistics.new(false) }
  let(:last_round) { 200 }

  context 'when there is no a valid game object' do
    let(:statistics) { nil }

    it { expect(statistics).to be_nil }
  end

  describe 'printed statistics after the game' do
    context 'with 1 player' do
      let(:player2) { nil }
      let(:number_of_players) { 1 }

      context 'using Matingala strategy' do
        let!(:player_strategy_number) { 1 }
        let(:player_strategy_options) { { options: nil } }

        include_examples 'with a specific strategy'
      end

      context 'using Waiting strategy' do
        let!(:player_strategy_number) { 2 }
        let(:player_strategy_options) do
          {
            times_to_wait: 3,
            combine_martingala: 2
          }
        end

        include_examples 'with a specific strategy'

        context 'combined with Martingala strategy' do
          let(:player_strategy_options) do
            {
              times_to_wait: 3,
              combine_martingala: 1
            }
          end

          include_examples 'with a specific strategy'
        end
      end

      context 'using 1 - 3 - 2 - 6 system strategy' do
        let!(:player_strategy_number) { 3 }
        let(:player_strategy_options) { { options: nil } }

        include_examples 'with a specific strategy'
      end

      context 'using Labouchere strategy' do
        let!(:player_strategy_number) { 4 }
        let(:player_strategy_options) do
          {
            range_starts: 1,
            range_ends: 9
          }
        end

        include_examples 'with a specific strategy'
      end
    end

    context 'with 2 players' do
      let(:number_of_players) { 2 }
      let!(:player2_atts) do
        {
          rounds: 10,
          from_main: false,
          name: 'Ali Clarkson',
          budget: 10_000,
          initial_bet: 50,
          strategy_number: player_strategy_number,
          strategy_options: player_strategy_options,
          bet_options: player_bet_options,
          bet_numbers: bet_numbers
        }
      end
      let(:player2) do
        Player.new(player2_atts)
      end

      context 'using Matingala strategy' do
        let(:player_strategy_number) { 1 }
        let(:player_strategy_options) { { options: nil } }

        include_examples 'with a specific strategy'
      end

      context 'using Waiting strategy' do
        let(:player_strategy_number) { 2 }
        let(:number_of_players) { 1 }
        let(:player_strategy_options) do
          {
            times_to_wait: 3,
            combine_martingala: 2
          }
        end

        include_examples 'with a specific strategy'

        context 'combined with Martingala strategy' do
          let(:player_strategy_options) do
            {
              times_to_wait: 3,
              combine_martingala: 1
            }
          end

          include_examples 'with a specific strategy'
        end
      end

      context 'using 1 - 3 - 2 - 6 system strategy' do
        let(:player_strategy_number) { 3 }
        let(:number_of_players) { 1 }
        let(:player_strategy_options) { { options: nil } }

        include_examples 'with a specific strategy'
      end

      context 'using Labouchere strategy' do
        let(:player_strategy_number) { 4 }
        let(:number_of_players) { 1 }
        let(:player_strategy_options) do
          {
            range_starts: 1,
            range_ends: 9
          }
        end

        include_examples 'with a specific strategy'
      end
    end
  end
end
