# frozen_string_literal: true

require 'spec_helper'
require 'byebug'
require './app/bet/bet'

RSpec.describe Bet, type: :model do
  let(:bet) { Bet.new }
  let(:bet_number) { nil }

  describe '#print_catalog' do
    it do
      expect { bet.print_catalog }.to output(
        "     (  1 ) Straight Bet     : is an inside_bet that pays ( 35 : 1 )\n" \
        "     (  2 ) Split Bet        : is an inside_bet that pays ( 17 : 1 )\n" \
        "     (  3 ) Street Bet       : is an inside_bet that pays ( 11 : 1 )\n" \
        "     (  4 ) Corner Bet       : is an inside_bet that pays (  8 : 1 )\n" \
        "     (  5 ) Five numbers Bet : is an inside_bet that pays (  6 : 1 )\n" \
        "     (  6 ) Line Bet         : is an inside_bet that pays (  5 : 1 )\n" \
        "     (  7 ) Range Bet        : is an outside_bet that pays (  2 : 1 )\n" \
        "     (  8 ) Column Bet       : is an outside_bet that pays (  2 : 1 )\n" \
        "     (  9 ) Low | High Bet   : is an outside_bet that pays (  1 : 1 )\n" \
        "     ( 10 ) Red | Black Bet  : is an outside_bet that pays (  1 : 1 )\n" \
        "     ( 11 ) Even | Odd Bet   : is an outside_bet that pays (  1 : 1 )\n"
      ).to_stdout
    end
  end

  describe '#print_hint_and_data_needed' do
    context 'when there is some data needed' do
      let(:bet_number) { 4 }

      it do
        expect { bet.print_hint_and_data_needed(bet_number) }.to output(
          "What are you going to be beting? -> 4 adjacent numbers (not 0 nor 00)\n" \
          "\n   Data needed:\n       " \
          "num1, num2, num3, num4,  \n"
        ).to_stdout
      end
    end

    context 'when there is no extra data needed' do
      let(:bet_number) { 5 }

      it do
        expect { bet.print_hint_and_data_needed(bet_number) }.to output(
          "What are you going to be beting? -> this bet includes 0, 00, 1, 2 and 3 numbers\n" \
          "\n   Data needed:\n        \n" \
        ).to_stdout
      end
    end
  end

  describe '#options_to_fill' do
    let(:bet_number) { 7 }
    let(:expected_result) do
      {
        which_twelve: nil,
        text: 'First, second or third group of twelve numbers'
      }
    end

    it { expect(bet.options_to_fill(bet_number)).to eq(expected_result) }
  end

  describe '#pay_rate' do
    let(:bet_number) { 2 }
    let(:expected_result) { 17 }

    it { expect(bet.pay_rate(bet_number)).to eq(expected_result) }
  end

  describe '#count' do
    let(:expected_result) { 11 }

    it { expect(bet.count).to eq(expected_result) }
  end

  describe '#get_block' do
    let(:bet_number) { 4 }
    let(:expected_result) { nil }

    it { expect(bet.get_block(bet_number)).to eq(expected_result) }
  end

  describe '#numbers_to_bet' do
    let(:bet_number) { 11 }
    let(:bet_proc) { subject.get_block(bet_number) }
    let(:bet_options) { { even_or_odd: 1 } }
    let(:expected_result) { [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36] }

    it { expect(bet.numbers_to_bet(bet_options, bet_proc)).to eq(expected_result) }
  end
end
