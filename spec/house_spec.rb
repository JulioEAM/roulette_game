# frozen_string_literal: true

require 'spec_helper'
require 'byebug'
require './app/house'
require './app/player'

RSpec.describe House, type: :model do
  subject { house }

  let(:house) { House.new }

  context '#still_can_pay?' do
    let(:expected_result) { true }

    it { expect(subject.still_can_pay?).to eq(expected_result) }
  end
end
