# frozen_string_literal: true

require 'spec_helper'
require 'byebug'
require './app/strategy/strategy'
require './app/strategy/labouchere'
require './app/strategy/sequence'

RSpec.describe Strategy, type: :model do
  let(:strategy) { Strategy.new(0) }

  describe '#print_catalog' do
    it do
      expect { strategy.print_catalog }.to output(
        '     (  1 ) Martingala      : Hint (Each lost round you must double your bet; at win ' \
        "your bet back to its initial val\n" \
        ") \n" \
        "     (  2 ) Waiting         : Hint (Only work with outside and 'Five number' bets.\n" \
        "     NOTE: You can use it only with outside bets and Five numbers bet) \n" \
        '     (  3 ) 1 - 3 - 2 - 6 system: Hint (Each lost round restar your bet to its ' \
        "initial val, each win multiply it by the next number in the system) \n" \
        '     (  4 ) Labouchere      : Hint (Also called Cancellation, little complicated but ' \
        'basically your bet is the summatory of the extremes from yout initial serial  numbers. ' \
        'Each win you cancel those and take the next ones as your next bet. If you loose, then ' \
        "you add the amount of money from your las bet.) \n"
      ).to_stdout
    end
  end

  describe '#print_hint_and_data_needed' do
    context 'when there is some data needed' do
      let(:strategy_number) { 2 }

      it do
        expect { strategy.print_hint_and_data_needed(strategy_number) }.to output(
          "What does the strategy do? -> Only work with outside and 'Five number' bets.\n" \
          "     NOTE: You can use it only with outside bets and Five numbers bet\n" \
          "  HINT >> How many times do you want to wait? ... and 1 to combine\n\n" \
          "   Data needed:\n       " \
          "times_to_wait, combine_martingala,  \n"
        ).to_stdout
      end
    end

    context 'when there is no extra data needed' do
      let(:strategy_number) { 1 }

      it do
        expect { strategy.print_hint_and_data_needed(strategy_number) }.to output(
          'What does the strategy do? -> Each lost round you must double your bet; at win ' \
          "your bet back to its initial val\n" \
          "  HINT >> No extra data needed\n" \
          "\n   Data needed:\n        \n"
        ).to_stdout
      end
    end
  end

  describe '#options_to_fill' do
    let(:strategy_number) { 2 }
    let(:expected_result) do
      {
        times_to_wait: nil,
        combine_martingala: nil,
        text: "How many times do you want to wait? ... and 1 to combine\n"
      }
    end

    it { expect(strategy.options_to_fill(strategy_number)).to eq(expected_result) }
  end

  describe '#count' do
    let(:expected_result) { 4 }

    it { expect(strategy.count).to eq(expected_result) }
  end

  describe '#what_should_player_do' do
    let(:options) do
      {
        range_starts: 10,
        range_ends: 20
      }
    end
    let(:strategy) { Labouchere.new(options) }
    let(:event) { '' }
    let(:bet) { 0 }
    let(:initial_bet) { 1 }

    let(:expected_result) { 30 }

    it do
      expect(
        strategy
        .what_should_player_do(event, bet, initial_bet)
      ).to eq(30)
    end
  end

  describe '#needs_preaction' do
    let(:options) { {} }
    let(:strategy) { Sequence.new(options) }

    it { expect(strategy.needs_preaction).to be false }
  end
end
