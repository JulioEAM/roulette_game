# frozen_string_literal: true

require_relative 'strategy/strategy'
require_relative 'strategy/martingala'
require_relative 'strategy/waiting'
require_relative 'strategy/sequence'
require_relative 'strategy/labouchere'

class Player
  attr_reader :name, :rounds
  attr_writer :from_main

  def initialize(params)
    @rounds = params[:rounds]
    @from_main = params[:from_main]
    fill_related_to_player(params)
    fill_related_to_bet(params)
    fill_related_to_strategy(params)
  end

  def play(user_interface, house, game)
    @rounds.times do |round|
      return real_round(round) unless still_in_game?

      calculate_bet(real_round(round)) if still_in_game?

      message = "Round #{real_round(round)} | Winning number -> #{winning_number = game.spin}"
      user_interface.print_message(message) if @from_main
      message = "Budget: #{@budget}, Curr_bet: #{@current_bet}, Next_bet: #{@next_bet}"
      user_interface.print_message(message) if @from_main

      money_for_house = give_or_take_money(winning_number, real_round(round))
      house.update_money(money_for_house)
      update_next_bet if still_in_game?
    end

    @rounds
  end

  private

  def real_round(round)
    round + 1
  end

  def fill_related_to_player(params)
    @name = params[:name]
    @budget = params[:budget].to_f
    @initial_budget = @budget
    @lost_times = 0
    @win_times = 0
    @current_bet = 0
    @next_bet = 0
    @last_event = ''
    @biggest_budget = 0.00
    @round_with_biggest_budget = 0
  end

  def fill_related_to_bet(params)
    @bet_number = params[:bet_number]
    @initial_bet = params[:initial_bet].to_f
    @bet_options = params[:bet_options]
    @bet_numbers = params[:bet_numbers]
  end

  def fill_related_to_strategy(params)
    @strategy_number = params[:strategy_number]
    @strategy_options = params[:strategy_options]
    @strategy = Strategy::STRATEGIES_NAMES[@strategy_number - 1].constantize.new(@strategy_options)
  end

  def give_or_take_money(winning_number, round)
    won = verify_if_won(winning_number)
    modify_budget(won, round)
    add_win_or_lost(won)
    return pay_if_won if won

    @current_bet * -1
  end

  def still_in_game?
    return false if @budget < 1

    true
  end

  def modify_budget(won, round)
    won && @current_bet != 0 ? @budget += pay_if_won : @budget -= @current_bet
    @round_with_biggest_budget = round if @budget > @biggest_budget
    @biggest_budget = @budget if @budget > @biggest_budget
  end

  def add_win_or_lost(won)
    if won
      @win_times += 1
      @last_event = 'won'
    else
      @lost_times += 1
      @last_event = 'lost'
    end
  end

  def pay_if_won
    @current_bet * @bet_options[:pay_rate]
  end

  def verify_if_won(winning_number)
    @bet_numbers.include?(winning_number)
  end

  def must_use_initial_bet(round)
    @current_bet.zero? && round == 1 && @next_bet.zero?
  end

  def waiting_the_right_moment
    @strategy_number == 2 && @next_bet.zero?
  end

  def calculate_bet(round)
    update_next_bet if @strategy.needs_preaction && round == 1
    return @current_bet = 0 if waiting_the_right_moment
    return @current_bet = @initial_bet if must_use_initial_bet(round)
    return @current_bet = @next_bet if @budget >= @next_bet

    @current_bet = @budget
  end

  def update_next_bet
    @next_bet = @strategy.what_should_player_do(@last_event, @current_bet, @initial_bet)
  end
end
