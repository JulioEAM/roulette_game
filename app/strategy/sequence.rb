# frozen_string_literal: true

require_relative 'strategy'

class Sequence < Strategy
  def initialize(player_options)
    super(player_options)
    @name = '1 - 3 - 2 - 6 system'
    @description = "Only work with outside and 'Five number' bets.\n" \
                   '     NOTE: You can use it only with outside bets and Five numbers bet'
    @options[:factors] = [1, 3, 2, 6].cycle
  end

  def what_should_player_do(event, _bet, initial_bet)
    return initial_bet * @options[:factors].next if event == 'won'

    @options[:factors].rewind.next
    initial_bet
  end
end
