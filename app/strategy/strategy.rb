# frozen_string_literal: true

require 'active_support/inflector' # Para extender String y poder singularize

class Strategy
  attr_reader :options

  STRATEGIES_NAMES = %w[Martingala Waiting Sequence Labouchere].freeze

  STRATEGIES_CATALOG = {
    '1' => { name: 'Martingala', description: 'Each lost round you must double your bet; at win ' \
             "your bet back to its initial val\n", options: { text: 'No extra data needed' } },
    '2' => { name: 'Waiting', description: "Only work with outside and 'Five number' bets.\n" \
                      '     NOTE: You can use it only with outside bets and Five numbers bet',
             options: { times_to_wait: nil, combine_martingala: nil,
                        text: "How many times do you want to wait? ... and 1 to combine\n" } },
    '3' => { name: '1 - 3 - 2 - 6 system', description: 'Each lost round restar your bet to its ' \
             'initial val, each win multiply it by the next number in the system',
             options: { text: 'No extra data needed' } },
    '4' => { name: 'Labouchere', description: 'Also called Cancellation, little complicated but ' \
             'basically your bet is the summatory of the extremes from yout initial serial ' \
             ' numbers. Each win you cancel those and take the next ones as your next bet. ' \
             'If you loose, then you add the amount of money from your las bet.',
             options: { range_starts: nil, range_ends: nil,
                        text: 'Series of consecutive numbers to bet' \
                              '          No need to fill numbers, that is something for us' } }
  }.freeze

  def print_catalog
    STRATEGIES_CATALOG.each do |index, strategy|
      puts "     ( #{index.rjust(2)} ) #{strategy[:name].to_s.ljust(16)}: " \
           "Hint (#{strategy[:description]}) "
    end
  end

  def print_hint_and_data_needed(strategy_number)
    puts "What does the strategy do? -> #{STRATEGIES_CATALOG[strategy_number.to_s][:description]}"
    puts "  HINT >> #{options_to_fill(strategy_number)[:text]}"
    print "\n   Data needed:\n       "
    options_to_fill(strategy_number).each do |option, _val|
      print "#{option}, " if option != :text
    end
    puts ' '
  end

  def count
    STRATEGIES_CATALOG.count
  end

  def options_to_fill(strategy_number)
    @options = STRATEGIES_CATALOG[strategy_number.to_s][:options]
  end

  def initialize(player_options)
    @name = 'Some strategy name'
    @description = 'Some description for the strategy'
    @pre_action_needed = false
    @options = player_options
  end

  def strategy_attributes
    {
      name: @name,
      description: @description,
      pre_action_needed: @pre_action_needed,
      options: @options
    }
  end

  def what_should_player_do(_event, _bet, _initial_bet)
    0
  end

  def needs_preaction
    @pre_action_needed
  end
end
