# frozen_string_literal: true

require_relative 'strategy'

class Waiting < Strategy
  def initialize(player_options)
    super(player_options)
    @name = 'Waiting'
    @description = "Only work with outside and 'Five number' bets.\n" \
                   '     NOTE: You can use it only with outside bets and Five numbers bet'
    @options[:losts_counter] = 0
  end

  def what_should_player_do(event, bet, initial_bet)
    if @options[:losts_counter] < @options[:times_to_wait]
      @options[:losts_counter] += 1 if event == 'lost'
      @options[:losts_counter] = 0 if event == 'won'
      if @options[:losts_counter] == @options[:times_to_wait]
        return actions_for_calculate_bet(bet, initial_bet)
      end

      return super
    end
    return bet * 2 if lost_and_combine_martingala(event)

    @options[:losts_counter] = 0
  end

  def actions_for_calculate_bet(bet, initial_bet)
    return bet = initial_bet if bet.zero?

    return bet * 2 if event == 'lost' && @options[:combine_martingala] == 1

    initial_bet
  end

  def lost_and_combine_martingala(event)
    event == 'lost' && @options[:combine_martingala] == 1
  end
end
