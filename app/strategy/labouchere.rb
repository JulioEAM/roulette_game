# frozen_string_literal: true

require_relative 'strategy'

class Labouchere < Strategy
  def initialize(player_options)
    super(player_options)
    @name = 'Labouchere'
    @description = 'Also called Cancellation, little complicated but ' \
                   'basically your bet is the summatory of the extremes from yout initial serial ' \
                   ' numbers. Each win you cancel those and take the next ones as your next bet. ' \
                   'If you loose, then you add the amount of money from your las bet.'
    @preaction_needed = true
    @options[:numbers] = (@options[:range_starts]..@options[:range_ends]).to_a
  end

  def what_should_player_do(event, _bet, _initial_bet)
    if event == 'won'
      remove_first_and_last_elements
      reset_numbers
    elsif event == 'lost'
      add_element
    end
    # p options
    @options[:numbers].first + @options[:numbers].last
  end

  def remove_first_and_last_elements
    @options[:numbers].shift
    @options[:numbers].pop if @options[:numbers].count.positive?
  end

  def array_from_range
    (@options[:range_starts]..@options[:range_ends]).to_a
  end

  def add_element
    @options[:numbers] << @options[:numbers].first + @options[:numbers].last
  end

  def reset_numbers
    @options[:numbers] = array_from_range if @options[:numbers].count.zero?
  end
end
