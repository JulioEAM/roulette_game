# frozen_string_literal: true

require_relative 'strategy'

class Martingala < Strategy
  def initialize(player_options)
    super
    @name = 'Martingala'
    @description = 'Each lost round you double your bet; at win your bet back to its initial val'
  end

  def what_should_player_do(event, bet, initial_bet)
    return bet * 2 if event == 'lost'

    initial_bet
  end
end
