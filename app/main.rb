# frozen_string_literal: true

require_relative 'user_interface'
require_relative 'player'
require_relative 'house'
require_relative 'bet/bet'
require_relative 'roulette/roulette'
require_relative 'statistics'
require 'byebug'

def say_hi
  puts '--------------------------------------------------------------'
  puts '--------------  Welcome to Roulette simulator!  --------------'
  puts '--------------------------------------------------------------'
end

def say_good_bye
  puts '--------------------------------------------------------------'
  puts '--------------         See you nex time         --------------'
  puts '--------------------------------------------------------------'
end

def say_wrong_values
  puts '--------------------------------------------------------------'
  puts '--------------   There where some wrong values  --------------'
  puts '--------------------------------------------------------------'
  puts " So we could not initialize necessary objects.\n Please correct them and try again."
end

def build_objects_without_data
  @bet = Bet.new
  @strategy = Strategy.new(0)
  @house = House.new
  @statistics = Statistics.new(true)
  @interface = UserInterface.new(@bet, @strategy)
  @values_for_classes = {}
end

def build_objects_with_data
  @players = {}

  1.upto(number_of_players) { |index| @players[index.to_s.to_sym] = create_new_player(index) }
#  fill_roulette_initial_attributes(players)
  @roulette_game = RouletteGame.new(@values_for_classes[:roulette_game])
end

def number_of_players
  @players_in_game = @values_for_classes[:roulette_game][:players_in_game]
end

#def fill_roulette_initial_attributes(players)
#  @values_for_classes[:roulette_game][:players] = players
#
#  @values_for_classes[:roulette_game][:house] = @house
#  @values_for_classes[:roulette_game][:strategy] = @strategy
#end

def create_new_player(index)
  Player.new(@values_for_classes[:players][index.to_s.to_sym])
end

def max_players
  @players_in_game < @players.count ? @players_in_game : @players.count
end

def player(index)
  @players[index.to_s.to_sym]
end

system('clear')
build_objects_without_data
say_hi

if (@values_for_classes = @interface.ask_for_initial_values).nil?
  say_wrong_values
elsif build_objects_with_data
  @interface.spinning_animation
  1.upto(max_players) do |index|
    if (last_round = player(index).play(@interface, @house, @roulette_game)) == -1
      @interface.all_players_are_broke(last_round)
    end
    player(index).from_main = true
    @statistics.print_statistics(@roulette_game, last_round, @house, @bet, @players)
  end
else
  say_wrong_values
end

say_good_bye
