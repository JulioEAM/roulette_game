# frozen_string_literal: true

class UserInterface
  attr_reader :values_for_classes

  def initialize(bet, strategy)
    @bet = bet
    @strategy = strategy
    @roulette_game = nil
    @values_for_classes = {}
  end

  def ask_for_initial_values
    if (values_for_classes[:roulette_game] = ask_for_game_values)
      return nil unless (values_for_classes[:players] = ask_for_players(number_of_players))
    end

    values_for_classes
  end

  def spinning_animation
    system('clear')
    print "\nSpinning"
    (1..5).each do |_times|
      sleep(0.1)
      print '.'
    end
    puts ' '
  end

  def all_players_are_broke(last_round)
    puts "All players lost all their money at round ##{last_round + 1} :( "
  end

  def print_message(message)
    puts message
  end

  private

  def number_of_players
    values_for_classes[:roulette_game][:players_in_game]
  end

  def ask_for_game_values
    roulette_params = {}

    return nil if (roulette_params[:players_in_game] = number_of_participants).nil?

    roulette_params
  end

  def number_of_participants
    puts "\nHow many participants do you want to play with?"
    return nil if (number = gets.chomp.to_i).zero?

    number
  end

  def number_of_rounds
    puts "\nHow many rounds do you want to play? (max 200 to simulate a few hours playing)"
    return nil if (number = gets.chomp.to_i).zero?
    return 200 if number > 200

    number
  end

  def ask_for_players(number_of_players)
    players_params = {}
    1.upto(number_of_players) do |index|
      return nil unless (players_params[index.to_s.to_sym] = ask_for_player_values(index))
    end
    players_params
  end

  def ask_for_player_values(player)
    player_params = {}

    player_params[:rounds] = number_of_rounds
    player_params[:from_main] = true
    attributes(player_params, player)
    player_params[:strategy_number] = ask_for_class(@strategy)
    player_params[:strategy_options] = ask_for_options(@strategy, player_params[:strategy_number])
    related_to_bet(player_params)

    player_params.each { |_param, value| return nil if value.nil? }

    player_params
  end

  def attributes(player_params, player)
    player_params[:name] = ask_for_attribute(player_number: player, attribute: 'Name')
    player_params[:budget] = ask_for_attribute(player_number: player, attribute: 'Budget').to_i
    player_params[:initial_bet] = ask_for_attribute(player_number: player,
                                                    attribute: 'Initial Bet Amount').to_i
  end

  def related_to_bet(player_params)
    player_params[:bet_number] = ask_for_class(@bet)
    bet_options(player_params)
  end

  def bet_options(player_params)
    bet_options = ask_for_options(@bet, player_params[:bet_number])
    player_params[:bet_options] = bet_options
    proc_to_pass = @bet.get_block(player_params[:bet_number])
    player_params[:bet_numbers] = @bet.numbers_to_bet(bet_options, proc_to_pass)
    player_params[:bet_options][:pay_rate] = @bet.pay_rate(player_params[:bet_number])

    player_params
  end

  def ask_for_attribute(options)
    puts "\nWhat is the #{options[:attribute]} for the player #{options[:player_number]}?"
    gets.chomp
  end

  def ask_for_class(class_name)
    puts "\n>>>> FOR YOUR #{class_name.class.name.upcase}:  <<<<"
    puts "What #{class_name.class.name.upcase} is going to use? (choose a number)\n"
    class_name.print_catalog
    number = gets.chomp.to_i
    if number < 1 || number > class_name.count
      puts "That is not a valid option for #{class_name}"
      return nil
    end
    number
  end

  def ask_for_options(object, number)
    options = {}

    if object.options_to_fill(number).count == 1
      puts object.options[:text]
      return options
    end
    object.print_hint_and_data_needed(number)
    each_option(object, options)

    options
  end

  def each_option(object, options)
    object.options.each do |option, _value|
      unless option == :text
        puts "What is the value you want for #{option}?"
        options[option] = gets.chomp.to_i
      end
    end
  end
end
