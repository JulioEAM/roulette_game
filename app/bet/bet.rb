# frozen_string_literal: true

require 'active_support/inflector' # Para extender String y poder singularize

class Bet
  attr_reader :options

  REDS = [1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36].freeze
  BLACKS = [2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35].freeze
  EVENS = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36].freeze
  UNEVENS = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35].freeze
  COLUMNS = {
    1 => [1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34],
    2 => [2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35],
    3 => [3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36]
  }.freeze

  BETS_CATALOG = {
    inside_bets: {
      '1' => { name: 'Straight Bet', pay_rate: 35,
               options: { number: nil, text: 'a number (1 - 36, 0 or 00)' }, block: nil },
      '2' => { name: 'Split Bet', pay_rate: 17,
               options: { number1: nil, number2: nil, text: '2 adjacent numbers (not 0 nor 00)' },
               block: nil },
      '3' => { name: 'Street Bet', pay_rate: 11,
               options: { row: nil, text: 'a row (1 - 12)' },
               block: proc { |row| [(row[0] * 3) - 2, (row[0] * 3) - 1, (row[0] * 3)] } },
      '4' => { name: 'Corner Bet', pay_rate: 8,
               options: { num1: nil, num2: nil, num3: nil, num4: nil,
                          text: '4 adjacent numbers (not 0 nor 00)' }, block: nil },
      '5' => { name: 'Five numbers Bet', pay_rate: 6,
               options: { text: 'this bet includes 0, 00, 1, 2 and 3 numbers' },
               block: proc { |_five_numbers| [37, 0, 1, 2, 3] } },
      '6' => { name: 'Line Bet', pay_rate: 5,
               options: { line: nil, text: '2 adjacent rows (1 to 11)' },
               block: proc do |row|
                        first_number = (row[0] * 3) - 2
                        [first_number, first_number + 1, first_number + 2,
                         first_number + 3, first_number + 4, first_number + 5]
                      end }
    },
    outside_bets: {
      '7' => { name: 'Range Bet', pay_rate: 2,
               options: { which_twelve: nil,
                          text: 'First, second or third group of twelve numbers' },
               block: proc do |which_twelve|
                        next (which_twelve[0]..(which_twelve[0] * 12)).to_a if which_twelve[0] == 1

                        ((((which_twelve[0] - 1) * 12) + 1)..(which_twelve[0] * 12)).to_a
                      end },
      '8' => { name: 'Column Bet', pay_rate: 2,
               options: { column: nil, text: 'Wich column do you want tu stand for? (1, 2 or 3)' },
               block: proc { |column| COLUMNS[column[0]] } },
      '9' => { name: 'Low | High Bet', pay_rate: 1,
               options: { low_or_high: nil, text: '1 (first 18 numbers), 2 (last 18 numbers) - ' },
               block: proc { |low| low[0] == 1 ? (1..18).to_a : (19..36).to_a } },
      '10' => { name: 'Red | Black Bet', pay_rate: 1,
                options: { red_or_black: nil, text: '1 (red numbers), 2 (black numbers)' },
                block: proc { |red_or_black| red_or_black[0] == 1 ? REDS : BLACKS } },
      '11' => { name: 'Even | Odd Bet', pay_rate: 1,
                options: { even_or_odd: nil, text: '1 (even numbers), 2 (odd numbers)' },
                block: proc { |even_or_odd| even_or_odd[0] == 1 ? EVENS : UNEVENS } }
    }
  }.freeze

  def print_catalog
    BETS_CATALOG.each do |kind, bets|
      bets.each do |index, bet|
        puts "     ( #{index.rjust(2)} ) #{bet[:name].ljust(17)}: is an #{kind.to_s.singularize} " \
             "that pays ( #{bet[:pay_rate].to_s.rjust(2)} : 1 )"
      end
    end
  end

  def print_hint_and_data_needed(bet_number)
    puts "What are you going to be beting? -> #{inside_or_outside_bet(bet_number)[:options][:text]}"
    print "\n   Data needed:\n       "
    inside_or_outside_bet(bet_number)[:options].each do |option, _val|
      print "#{option}, " if option != :text
    end
    puts ' '
  end

  def options_to_fill(bet_number)
    @options = inside_or_outside_bet(bet_number)[:options]
  end

  def pay_rate(bet_number)
    inside_or_outside_bet(bet_number)[:pay_rate]
  end

  def count
    BETS_CATALOG[:inside_bets].count + BETS_CATALOG[:outside_bets].count
  end

  def get_block(bet_number)
    inside_or_outside_bet(bet_number)[:block]
  end

  def numbers_to_bet(bet_options, proc = nil)
    return proc.call(bet_options.values) if proc

    bet_options.values
  end

  private

  def inside_or_outside_bet(bet_number)
    BETS_CATALOG[:inside_bets][bet_number.to_s] || BETS_CATALOG[:outside_bets][bet_number.to_s]
  end
end
