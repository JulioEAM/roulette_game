# frozen_string_literal: true

class RouletteGame
  def initialize(params)
    @players_in_game = params[:players_in_game]
  end

  def spin
    rand(0..37)
  end
end
