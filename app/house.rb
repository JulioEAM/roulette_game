# frozen_string_literal: true

class House
  def initialize
    @money_on_house = 1_000_000.00
    @initial_money = @money_on_house
    @still_can_pay = true
  end

#  def pay_or_collect(players_in_game, players, winning_number, round)
#    1.upto(players_in_game) do |time|
#      if players[time.to_s.to_sym].still_in_game?
#        @money_on_house -= players[time.to_s.to_sym].give_or_take_money(winning_number, round)
#      end
#    end
#  end

  def update_money(money)
    @money_on_house -= money
  end

  def still_can_pay?
    false if @money_on_house < 1
    true
  end
end
