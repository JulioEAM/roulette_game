# frozen_string_literal: true

class Statistics
  def initialize(from_main = false)
    @from_main = from_main
  end

  def print_statistics(game, last_round, house, bet, players)
    return if game.nil?

    print_begin
    players_in_game = from_instance(game, 'players_in_game')
    print_game_numbers(game, last_round, players, players_in_game)
    print_house_numbers(house)
    iterate_players(players, bet, players_in_game)
    print_end
    'Hola'
  end

  private

  def print_message(message)
    puts message if @from_main
  end

  def print_begin
    message = "--------------------------------------------------------------\n" \
              "-----------    Here is what happend in the game    -----------\n" \
              '--------------------------------------------------------------'
    print_message(message)
  end

  def print_end
    message = "--------------------------------------------------------------\n" \
              "---------   Those were the statistics por the game    --------\n" \
              '--------------------------------------------------------------'
    print_message(message)
  end

  def print_game_numbers(game, last_round, players, players_in_game)
    rounds_setted = from_instance(game, 'rounds')
    message = "-----------    General Roulette Game numbers       -----------\n" \
              "#{rounds_setted} Rounds were setted from the begining\n" \
              "#{last_round} was the last round played\n" \
              "#{players_lost(players, last_round, players_in_game)}\n" \
              "#{from_instance(game, 'players_in_game')} players were betting at the same time\n"
    print_message(message)
  end

  def print_house_numbers(house)
    message = "-----------            House numbers              -----------\n" \
              "#{print_money(initial_money = from_instance(house, 'initial_money'))} " \
              "was the money starts with\n" \
              "#{print_money(now_money = from_instance(house, 'money_on_house'))} " \
              "is the money it has now\n" \
              "#{earn_or_lost_money(initial_money, now_money, 'House')}\n"
    message << 'House losts all its money :O' unless house.still_can_pay?
    print_message(message)
  end

  def iterate_players(players, bet, players_in_game)
    1.upto(players_in_game) do |index|
      print_player_numbers(players[index.to_s.to_sym], index, bet)
    end
  end

  def print_player_numbers(player, index, _bet)
    message = "------- Numbers for Player #{index}: '#{from_instance(player, 'name')}'  -------\n" \
              "#{player_bet(player)}\n" \
              "#{player_strategy(player)}\n" \
              "#{player_data(player)}\n"
    print_message(message)
  end

  def from_instance(instance, variable)
    instance.instance_variable_get('@' + variable)
  end

  def players_lost(players, last_round, players_in_game)
    1.upto(players_in_game) do |index|
      "Player #{index} is broken" if last_round < player(index, players).rounds
    end
  end

  def earn_or_lost_money(initial_money, now_money, who)
    message = "...means #{who} earned money" if now_money > initial_money
    message = "...means #{who} lost money" if initial_money > now_money
    message
  end

  def player_bet(player)
    "Uses #{from_instance(player, 'bet_number')} kind of bet\n" \
    "Bets for numbers: #{from_instance(player, 'bet_numbers')}\n"
  end

  def player_strategy(player)
    "Uses #{from_instance(player, 'strategy_number')} strategy\n" \
    "With options: #{from_instance(player, 'strategy_options')}\n"
  end

  def player_data(player)
    "Starts with a budget of: \n" \
    "#{print_money(initial_budget = from_instance(player, 'initial_budget'))}\n" \
    "Ends with a budget of: #{now_budget = from_instance(player, 'budget')}" \
    "#{earn_or_lost_money(initial_budget, now_budget, 'Player')}\n" \
    "Wons #{wons = from_instance(player, 'win_times')} times\n" \
    " and Losts #{losts = from_instance(player, 'lost_times')} times\n" \
    "#{what_happened_more(wons, losts)}\n" \
    "#{biggest_budget(player)}\n"
  end

  def what_happened_more(wons, losts)
    message = 'So Won more times than she/he Lost' if wons > losts
    message = 'So Lost more times than she/he Won' if wons < losts
    message
  end

  def biggest_budget(player)
    "#{from_instance(player, 'round_with_biggest_budget')} was her/his best round\n" \
    "#{from_instance(player, 'biggest_budget')} was her/his biggest budget :O\n"
  end

  def player(index, players)
    players[index.to_s.to_sym]
  end

  def print_money(num)
    num.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\\1,').reverse
  end
end
